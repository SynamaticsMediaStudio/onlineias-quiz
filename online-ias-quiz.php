<?php 
/**
 * Plugin Name: OnlineIAS Quiz
 * Plugin Name: WordPress.org Plugin
 * Plugin URI:  https://onlineias.com/
 * Description: Quiz Plugin for OnlineIAS
 * Version:     1.0
 * Author:      Vishnu Raj
 * Author URI:  https://about.me/synamatics/
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */